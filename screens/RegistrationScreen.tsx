import React from 'react';
import {
	View,
	Text,
	Button
} from 'react-native';

export default function RegistrationScreen({route, navigation}){
	const {username} = route.params;

	return(
			<View style={{flex: 1, backgroundColor: '#1c2732', marginTop: 25}}>
				<View style={{flexDirection: 'row', padding: 15, backgroundColor: '#222e3d'}}>
					<View style={{flex: 5,}}>
						<Text style={{color: 'white', fontSize: 20}}>Your Phone</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
						<Text style={{color: 'white', fontSize: 20}}>Shield</Text>
					</View>
					<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
						<Text style={{color: 'white', fontSize: 20}}>World</Text>
					</View>
				</View>
				<View style={{flexDirection: 'column'}}>
				</View>
			</View>
		);
}