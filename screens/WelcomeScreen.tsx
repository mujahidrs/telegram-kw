import React from 'react';
import {
  View,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  Image
} from 'react-native';

export default function WelcomeScreen({props, navigation}){
  return(
      <View style={{flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center',
        backgroundColor: '#1c2732'
      }}>
        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
          <Image 
            source={{uri: "https://assets.stickpng.com/images/5842a8fba6515b1e0ad75b03.png"}}
            style={{width: 150, height: 150}}
          />
        </View>
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <Text style={{color: 'white', fontSize: 20, textAlign: 'center', fontWeight: 'bold'}}>Telegram KW</Text>
          <Text style={{color: 'white', fontSize: 15, textAlign: 'center'}}>The world's fastest messaging app.</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end', flex: 1}}>
          <TouchableOpacity
            style={{
            margin: 20,
          }}
            onPress={()=>navigation.navigate("Registration", {username: "mujahidrs"})}
          >
            <Text style={{color: '#53aad1', fontSize: 20, fontWeight: 'bold'}}>Start Messaging</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
}